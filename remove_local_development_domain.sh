#!/bin/bash

if [ "$EUID" -ne 0 ]
then
  echo "will run commands as sudo"
  sleep 5
fi

domain=$1
if [ -z "${domain}" ]
then
  echo "usage: $0 doman" >&2
  exit
fi

if grep "${domain}" /etc/hosts > /dev/null 2>&1
then
  echo "removing ${domain} entry from /etc/hosts" >&2
  sudo sed -i.bak "/${domain}/d" /etc/hosts
  sudo rm /etc/hosts.bak
fi
