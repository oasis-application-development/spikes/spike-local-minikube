#!/bin/bash

check_required_software() {
  if ! command VBoxManage -version
  then
    echo "Please Install VirtualBox" >&2
    echo "https://www.virtualbox.org/wiki/Downloads" >&2
    exit 1
  fi

  if ! command kubectl > /dev/null 2>&1
  then
    echo "please install kubectl and helm" >&2
    echo "https://kubernetes.io/docs/tasks/tools/" >&2
    echo "https://helm.sh/docs/intro/install/" >&2
    exit 1
  fi

  if ! command helm > /dev/null 2>&1
  then
    echo "please install helm" >&2
    echo "https://helm.sh/docs/intro/install/" >&2
    exit 1
  fi
}

install_minikube() {
  if ! command minikube > /dev/null 2>&1
  then
    echo "installing minikube" >&2
    curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-darwin-amd64
    sudo install minikube-darwin-amd64 /usr/local/bin/minikube
  fi
}

start_minikube() {
  if [ "$(minikube status -f '{{ .Host }}')" != "Running" ]
  then
    echo "starting minikube" >&2
    minikube start --embed-certs --feature-gates=EphemeralContainers=true --driver=virtualbox --addons=ingress-dns,ingress
  fi
}

run_main() {
    echo "installing for ${USER}" >&2
    check_required_software
    install_minikube
    start_minikube
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]
then
  run_main
fi
