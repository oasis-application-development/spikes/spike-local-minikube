Working with Minikube

## Installation

Install hyperkit
```
brew install hyperkit
```

[minikube start](https://minikube.sigs.k8s.io/docs/start/)

```bash
minikube start --addons=ingress-dns,ingress --driver=hyperkit --embed-certs --feature-gates=EphemeralContainers=true
```

### Issues

Hyperkit does not mount /Users by default. To use HostPath (or docker volumes), you must mount your home directory into the VM by running the following in a separate terminal:
```
minikube mount $HOME:$HOME
```
You have to leave this running, either in the foreground, or as a background process in your terminal.

Alternatively, you can install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) and change the `--driver` to virtualbox. Virtualbox mounts /Users by default. Virtualbox may perform better than hyperkit.

DUHS Cisco Anyconnect clobbers local network access (always has)
You can install openconnect on a mac using brew (I had to wrangle with directory permissions a lot, but finally got it to install)
```
brew install openconnect
```

Then I could connect to the vpn in a separate terminal:
```
sudo openconnect --user=$(ud -un) vpn.duhs.duke.edu
password: $netidpass
password: push (or click yubikey, maybe even fingerprint)
...
```
You have to leave this running, either in the foreground, or as a background process in your terminal.

ZScaler Causes Problems while connected to DHE VPN for Mac Users

See this [KB Article](https://duke.service-now.com/kb_view.do?sys_kb_id=0913eb641b3c78d0f9b56397624bcbda)
and download the cert using the link on the page.

The upshot is that there is an extra step reqiured allow minishift and your containers running on your workstation attached to the DHE VPN
to communicate with applications behind the DHE firewall. To allow minikube, and all containers/pods running on it, to access the internet,
installing the zscaler cert into ~/.minikube/certs on your mac host filesystem before you start minikube the first time, and then ensure you
start minikube (first time only, you can minikube stop and start without flags and it will continue to work, unless you minikube delete)
with the `--embed-certs` option.
[Documentation](https://minikube.sigs.k8s.io/docs/handbook/untrusted_certs/)

#### Mount a HostPath

You can mount a local directory on your host machine into a container running in minikube with the virtualbox driver, using its full path definition (e.g.
you cannot use ~/, ../, etc.). This is useful for live reloading of code that you are editing on a host application such as VSCode, or for capturing output of code generator output such as rails g.
See the [Documentation](https://kubernetes.io/docs/concepts/storage/volumes/#hostpath-configuration-example).

An example has been provided in this repo. Note, this will run the pod with spec.containers[0].securityContext.runAsUser set to 1000, which is the id of the
docker user on the minikube Virtualbox machine. The VM is designed to map permissions on files/directories created within the VM to your host user
automatically. This way files created in the container use the same id to create files/directories in the pod as outside.

```bash
sed "s|PWD|$(pwd)|" test-hostpath.yaml | kubectl create -f -
kubectl attach -ti test-hostpath
ls /mounted
echo "from the container" >> /mounted/test.from.inside
exit
ls -l test.from.inside
cat test.from.inside
echo "from the host" >> test.from.inside
kubectl attach -ti test-hostpath
cat /mounted/test.from.inside
rm /mounted/test.from.inside
exit
ls -l /tmp/test-hostpath
sed "s|PWD|$(pwd)|" test-hostpath.yaml | kubectl delete -f -
```

If this does not work, check the output of the command `id -u` in the minikube vm:
```bash
minikube ssh
id -u
```

#### Host Local Development Application on a dedicated url for your project

Minikube comes with an Ingress Controller which is enabled usig the addons (see above).
This allows you to create an [Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/) object in your
application (similar to an Openshift Route) configured to route all requests to the k3s DNS with a Host matching a
Ingress spec.rules[].host entry. You will also need to configure your host machine to route requests to the host to the
minikube DNS. The configure_local_development_domain.sh script uses minikube ip to get the IP address of the minikube DNS, and registers
a domain to this IP in your systems /etc/hosts. There are other ways to do this, but this is pretty easy. More than one domain
can be mapped to the same IP. The remove_local_development_domain.sh script removes an entry from /etc/hosts matching the domain.
Both of these scripts require sudo to run

```bash
sudo ./configure_local_development_domain.sh web-server.oasis.local.development
kubectl create -f test-web-ingress.yaml
curl web-server.oasis.local.development
<html><body><h1>It works!</h1></body></html>
kubectl delete -f test-web-ingress.yml
sudo ./remove_local_development_domain.sh web-server.oasis.local.development
```

### Cleanup minikube

```bash
minikube delete --all --purge
```

### Install Opensource Docker CE and use it with minikube
You can install docker with brew
```
brew install docker
```

Once installed, run the following to connect docker to your minikube
```
eval $(minikube docker-env)
```

Minikube does not support the localhost requests to containers like docker desktop. You will need to get the minikube ip to connect to containers that it hosts:
```
minikube ip
```

You can create an entry in /etc/hosts (similar to what ./configure_local_development_domain.sh does) to create a stable url (you will have to update this each time you restart minikube).
```
sudo echo "$(minikube ip) $project.oasis.local.development" | sudo tee -a /etc/hosts
```
where project is the name of a project (you can have multiple entries/project)

***Warning*** You now have access to the images and containers that run kubernetes in minikube. DO NOT remove
any containers or images unless you really know what you are removing.