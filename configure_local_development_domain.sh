#!/bin/bash

if [ "$EUID" -ne 0 ]
then 
  echo "This will run commands as sudo"
  sleep 5
fi

domain=$1
if [ -z "${domain}" ]
then
  echo "usage: $0 domain" >&2
  exit
fi

minikube_ip=$(minikube ip)
if ! grep "${minikube_ip}.*${domain}" /etc/hosts > /dev/null 2>&1
then
  if grep "${domain}" /etc/hosts > /dev/null 2>&1
  then
    echo "removing old hosts entry" >&2
    sudo sed -i "/${domain}/d" /etc/hosts
  fi
  echo "adding ${domain} to /etc/hosts"
  echo "${minikube_ip} ${domain}" | sudo tee -a /etc/hosts
fi
